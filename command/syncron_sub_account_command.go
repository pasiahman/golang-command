package command

import (
	"fmt"

	"bitbucket.org/otten/otten-command/repository"
	"bitbucket.org/otten/otten-command/service"
	"github.com/urfave/cli"
)

type (
	SyncronSubAccountCommand interface {
		Run() cli.Command
	}
	syncronSubAccountCommand struct {
		ottenUserRepo            repository.OttenUserRepository
		syncronSubAccountService service.SyncronSubAccount
	}
)

func NewSyncronSubAccountCommand(ottenUserRepo repository.OttenUserRepository, syncronSubAccountService service.SyncronSubAccount) SyncronSubAccountCommand {
	return syncronSubAccountCommand{
		ottenUserRepo:            ottenUserRepo,
		syncronSubAccountService: syncronSubAccountService,
	}
}

func (our syncronSubAccountCommand) Run() cli.Command {
	// we create our commands
	return cli.Command{
		Name:  "sync_account_subscription",
		Usage: "Syncron data account user ecommerce and subscription",
		// the action, or code that will be executed when
		// we execute our `ns` command
		Action: func(c *cli.Context) error {
			err := our.syncronSubAccountService.Sync()
			fmt.Println(err)
			return nil
		},
	}
}
