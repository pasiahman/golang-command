package application

import (
	"bitbucket.org/otten/otten-command/command"
	"bitbucket.org/otten/otten-command/repository"
	"bitbucket.org/otten/otten-command/service"
	"github.com/urfave/cli"
)

var Commands []cli.Command

func (a *application) cliServiceList() {
	// repository
	ottenUserRepo := repository.NewOttenUserRepository(a.ottenDB)
	subsUserRepo := repository.NewSubsUserRepository(a.subsDB)

	// service
	syncronSubAccountService := service.NewSyncronSubAccountService(ottenUserRepo, subsUserRepo)

	// command
	Commands = []cli.Command{
		command.NewLooksUpIPAddressCommand().Run(),  // contoh command
		command.NewLooksUpNameServerCommand().Run(), // contoh command
		command.NewSyncronSubAccountCommand(ottenUserRepo, syncronSubAccountService).Run(),
	}

	a.cliApp.Commands = Commands
}
