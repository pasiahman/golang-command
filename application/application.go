package application

import (
	"context"
	"fmt"
	"os"
	"sync"

	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

type (
	// Application Interface
	Application interface {
		Run()
	}

	application struct {
		accountDB *sqlx.DB
		ottenDB   *sqlx.DB
		subsDB    *sqlx.DB
		cliApp    *cli.App
	}
)

// NewApplication :nodoc:
func NewApplication() Application {
	app := &application{}
	app.cliApp = cli.NewApp()
	app.cliApp.Name = "Otten Command CLI"
	app.cliApp.Usage = "Let's you query Servers!"

	return app
}

func connectDB(dsn string) (*sqlx.DB, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	db, err := sqlx.ConnectContext(ctx, "postgres", dsn)

	defer cancel()

	if err != nil {
		// cancel()
		return nil, err
	}

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	maxIdle := viper.GetInt("application.db.max_idle")
	maxOpen := viper.GetInt("application.db.max_conn")

	db.SetMaxIdleConns(maxIdle)
	db.SetMaxOpenConns(maxOpen)
	db.SetConnMaxLifetime(time.Minute * 1)

	return db, nil
}

func (a *application) connectDBOtten() error {
	db, err := connectDB(viper.GetString("otten.db.url"))

	if err != nil {
		log.WithFields(log.Fields{
			"service":  "account",
			"database": "otten",
		}).Error(err)
		return err
	}
	a.ottenDB = db

	log.WithFields(log.Fields{
		"service":  "account",
		"database": "otten",
	}).Info("Connected")

	return nil
}

func (a *application) connectAppDB() error {
	db, err := connectDB(viper.GetString("auth.db.url"))
	if err != nil {
		log.WithFields(log.Fields{
			"service":  "account",
			"database": "account",
		}).Error(err)
		return err
	}

	log.WithFields(log.Fields{
		"service":  "account",
		"database": "account",
	}).Info("Connected")

	a.accountDB = db
	return nil
}

func (a *application) connectSubsDB() error {
	db, err := connectDB(viper.GetString("subscription.db.url"))
	if err != nil {
		log.WithFields(log.Fields{
			"service":  "account",
			"database": "subscription",
		}).Error(err)
		return err
	}

	log.WithFields(log.Fields{
		"service":  "account",
		"database": "subscription",
	}).Info("Connected")

	a.subsDB = db
	return nil
}

// serve :nodoc:
func (a *application) serve() error {
	var wg sync.WaitGroup

	dbCount := 3
	errCh := make(chan error, dbCount)

	wg.Add(dbCount)

	go func() {
		defer wg.Done()
		// log.WithFields(log.Fields{
		// 	"service":  "account",
		// 	"database": "otten",
		// }).Info("Connecting")
		errCh <- a.connectDBOtten()
	}()

	go func() {
		defer wg.Done()
		// log.WithFields(log.Fields{
		// 	"service":  "account",
		// 	"database": "account",
		// }).Info("Connecting")
		errCh <- a.connectAppDB()
	}()

	go func() {
		defer wg.Done()
		// log.WithFields(log.Fields{
		// 	"service":  "account",
		// 	"database": "subscription",
		// }).Info("Connecting")
		errCh <- a.connectSubsDB()
	}()

	wg.Wait()

	for i := 0; i < dbCount; i++ {
		ec := <-errCh
		if ec != nil {
			close(errCh)
			log.Fatal(ec)
		}
	}

	fmt.Println("---------------------------------------------------------------------------------------------------------")
	fmt.Println("---------------------------------------------------------------------------------------------------------")

	a.cliServiceList()

	// Close error channel
	close(errCh)

	return nil
}

func (a *application) Run() {
	err := a.serve()
	if err != nil {
		log.Fatal(err)
	}

	// start our application
	err = a.cliApp.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
