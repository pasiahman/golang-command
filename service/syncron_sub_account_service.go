package service

import (
	"fmt"

	"bitbucket.org/otten/otten-command/model"
	"bitbucket.org/otten/otten-command/repository"
)

type (
	SyncronSubAccount interface {
		Sync() error
	}
	syncronSubAccount struct {
		ottenUserRepo repository.OttenUserRepository
		subsUserRepo  repository.SubsUserRepository
	}
)

func NewSyncronSubAccountService(ottenUserRepo repository.OttenUserRepository, subsUserRepo repository.SubsUserRepository) SyncronSubAccount {
	return syncronSubAccount{
		ottenUserRepo: ottenUserRepo,
		subsUserRepo:  subsUserRepo,
	}
}

func (s syncronSubAccount) Sync() error {
	var ottenUsers []model.OttenUser
	ottenUsers, _ = s.ottenUserRepo.SelectAllAccount()

	countData := len(ottenUsers)
	fmt.Println("total data ", countData)
	for _, ottenUser := range ottenUsers {
		s.subsUserRepo.UpdateOttenIDByEmail(ottenUser)
	}
	return nil
}
