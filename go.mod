module bitbucket.org/otten/otten-command

go 1.13

require (
	github.com/go-playground/validator/v10 v10.3.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.16 // indirect
	github.com/lib/pq v1.0.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	github.com/urfave/cli v1.22.4
)
