package main

import (
	"strings"

	"bitbucket.org/otten/otten-command/application"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func init() {
	viper.AutomaticEnv()
	viper.SetConfigType("yaml")

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Set default
	viper.SetDefault("application.db.max_idle", 5)
	viper.SetDefault("application.db.max_conn", 10)
	viper.SetConfigFile(`config.yml`)
	err := viper.ReadInConfig()
	if err != nil {
		logrus.Error(err)
	}
}

func main() {
	app := application.NewApplication()
	app.Run()
}
