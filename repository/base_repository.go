package repository

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

type (
	Repository interface {
		BeginTx() *sqlx.Tx
		GetContext() (context.Context, context.CancelFunc)
	}
	BaseRepository struct {
		DB *sqlx.DB
	}

	BuilderQueryString struct {
		SetParams   string
		Where       string
		Arg         interface{}
		ottUserRepo ottenUserRepository
	}

	BuildQuery interface {
		UpdateNew() error
	}
)

func (buildQuery BuilderQueryString) updateNew(arg interface{}, query string, tx *sqlx.Tx) error {
	if tx == nil {
		tx = buildQuery.ottUserRepo.BeginTx()
	}

	ctx, cancel := buildQuery.ottUserRepo.GetContext()
	defer cancel()

	query, params, err := tx.BindNamed(query, arg)
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, query, params...)
	if err != nil {
		return err
	}

	return nil
}

func (buildQuery BuilderQueryString) UpdateNew() error {
	query := `
		update ` + buildQuery.ottUserRepo.Table + `
		set
			` + buildQuery.SetParams + `
		where
			` + buildQuery.Where + `
		returning id
	`

	tx := buildQuery.ottUserRepo.DB.MustBegin()
	err := buildQuery.updateNew(buildQuery.Arg, query, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (b BaseRepository) BeginTx() *sqlx.Tx {
	return b.DB.MustBegin()
}

func (b BaseRepository) GetContext() (context.Context, context.CancelFunc) {
	c := context.Background()
	contextTimeout := time.Second * 2
	return context.WithTimeout(c, contextTimeout)
}

func BuidQueryString(data map[string]interface{}) string {
	var params []string
	for key := range data {
		param := fmt.Sprintf("%s=:%s", key, key)
		params = append(params, param)
	}
	return strings.Join(params, ", ")
}

func MergeData(a map[string]interface{}, b map[string]interface{}) map[string]interface{} {
	for k, v := range b {
		a[k] = v
	}
	return a
}
