package repository

import (
	"bitbucket.org/otten/otten-command/model"
	"github.com/jmoiron/sqlx"
)

type (
	OttenUserRepository interface {
		Repository
		SelectAllAccount() ([]model.OttenUser, error)
	}
	ottenUserRepository struct {
		BaseRepository
		Table string
	}
)

// NewOttenUserRepository :nodoc:
func NewOttenUserRepository(db *sqlx.DB) OttenUserRepository {
	r := ottenUserRepository{}
	r.DB = db
	r.Table = "user_account"

	return r
}

func (u ottenUserRepository) SelectAllAccount() ([]model.OttenUser, error) {
	query := `SELECT email, id FROM ` + u.Table + ``
	ottenUser := []model.OttenUser{}

	err := u.DB.Select(&ottenUser, query)
	if err != nil {
		return ottenUser, err
	}

	return ottenUser, nil
}
